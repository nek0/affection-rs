pub mod graphics;
pub use graphics::{Graphics, Event};

pub struct Config {
    window_title: String,
    window_size: (u32, u32),
}

impl Default for Config {
    fn default() -> Self {
        Config {
            window_title: "Affection".to_owned(),
            window_size: (800, 600),
        }
    }
}

pub trait Callbacks {
    fn load_state() -> Self;
    fn pre_loop(&mut self) {
    }
    fn handle_event(&mut self, ev: Event);
    fn update(&mut self);
    fn draw(&self, graphics: &mut Graphics);
    fn clean_up(self);
    fn do_next_step(&self) -> bool;
}

pub fn run<C: Callbacks>(config: Config) {
    let mut graphics = Graphics::new(&config);

    let mut state = C::load_state();
    state.pre_loop();
    while state.do_next_step() {
        state.update();
        for ev in graphics.get_events() {
            state.handle_event(ev);
        }
        state.draw(&mut graphics);
    }
    state.clean_up();
}

#[cfg(test)]
mod tests {
    use super::{Config, Callbacks, Event, Graphics};
    use crate::run;

    struct TestCallbacks {
        stepper: u32,
    }

    impl Callbacks for TestCallbacks {
        fn load_state() -> Self {
            TestCallbacks{
                stepper: 0,
            }
        }
        //fn preLoop(&mut self){}
        fn handle_event(&mut self, ev: Event){
        }
        fn update(&mut self){
            self.stepper = self.stepper + 1;
        }
        fn draw(&self, graphics: &mut Graphics){
        }
        fn clean_up(self){
        }
        fn do_next_step(&self) -> bool{
            self.stepper < 999999
        }
    }

    #[test]
    fn it_works() {
        let config = Config::default();
        run::<TestCallbacks>(config);
    }
}
