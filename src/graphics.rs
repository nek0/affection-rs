use sdl2::{video::Window, EventPump, event::EventPollIterator};
pub use sdl2::event::Event;
use super::Config;

pub struct Graphics {
    events: EventPump,
    window: Window,
}

impl Graphics {
    pub fn new(config: &Config) -> Self {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();
        let mut window = video_subsystem.window(
            &config.window_title,
            config.window_size.0,
            config.window_size.1
        )
            .opengl()
            .build()
            .unwrap();
        window.show();

        Graphics {
            events: sdl_context.event_pump().unwrap(),
            window,
        }
    }

    pub fn get_events(&mut self) -> EventPollIterator {
        self.events.poll_iter()
    }
}
